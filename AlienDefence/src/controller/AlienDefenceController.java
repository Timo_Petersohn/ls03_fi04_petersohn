package controller;

import model.Level;
import model.User;
import model.persistance.IPersistance;

public class AlienDefenceController {
	
	//Teilcontroller
	private GameController gameController;
	private LevelController levelController;
	private TargetController targetController;
	private AttemptController attemptController;
	private UserController userController;
	//TODO UserController implementieren
	
	//Persistenz
	private IPersistance alienDefenceModel;

	public AlienDefenceController(IPersistance alienDefenceModel) {
		super();
		this.alienDefenceModel = alienDefenceModel;
		this.attemptController = new AttemptController(alienDefenceModel);
		this.levelController = new LevelController(alienDefenceModel);
		this.targetController = new TargetController(alienDefenceModel);
		this.userController = new UserController(alienDefenceModel);
	}

	public IPersistance getAlienDefenceModel() {
		return alienDefenceModel;
	}

	public AttemptController getAttemptController() {
		return attemptController;
	}

	public LevelController getLevelController() {
		return levelController;
	}

	public TargetController getTargetController() {
		return targetController;
	}
	
	public UserController getUserController() {
		return userController;
	}

	public GameController startGame(Level selectedLevel, User user) {
		this.gameController = new GameController(selectedLevel, user, this);
		return this.gameController;
	}

	
}
