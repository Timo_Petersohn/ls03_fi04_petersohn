package view.menue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.GameController;
import controller.LevelController;
import controller.AlienDefenceController; 
import model.Level;
import model.User;
import view.game.GameGUI;



@SuppressWarnings("serial")
public class LevelChooser extends JPanel {
	
	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;
	private AlienDefenceController alienDefenceController;
	private User user;
	private boolean startGame;
	
	
	
	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(LevelController lvlControl, LeveldesignWindow leveldesignWindow, AlienDefenceController alienDefenceController, User user, boolean startGame, String knopf) {
		this.lvlControl = alienDefenceController.getLevelController();
		this.leveldesignWindow = leveldesignWindow;
		this.alienDefenceController = alienDefenceController;
		this.startGame = startGame;
		

		setLayout(new BorderLayout());

		JPanel pnlButtons = new JPanel();
		add(pnlButtons, BorderLayout.SOUTH);

		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});
		pnlButtons.add(btnNewLevel);

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);
		
		JButton btnStartGame = new JButton("Spiel starten");
		btnStartGame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStartGame_Clicked(alienDefenceController, user);
			}
		});
		pnlButtons.add(btnStartGame);

		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();
		
		
		if(knopf.equals("Text-Window")) {
			btnNewLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
		}else if(knopf.equals("LvLEditor-Window")) {
			btnStartGame.setVisible(false);
		}
	}
	
	

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
	
	public void btnStartGame_Clicked(AlienDefenceController alienDefenceController, User user) {
			
			int level_id = Integer 
					.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
			
			
			Level level = alienDefenceController.getLevelController().readLevel(level_id);
			
			
			Thread t = new Thread("GameThread") {
				@Override
				public void run() {

					GameController gameController = alienDefenceController.startGame(level, user);
					new GameGUI(gameController).start();
				}
			};
			t.start(); 
		
		
	}
}
