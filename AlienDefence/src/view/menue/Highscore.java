package view.menue;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import controller.AttemptController;
import model.Level;

@SuppressWarnings("serial")
public class Highscore extends JFrame {

	// Attribute
	private AttemptController attemptController;
	private int level_id;

	public Highscore(AttemptController attemptController, int level_id) {
		this(attemptController, level_id, 0);
	}
	public Highscore(AttemptController attemptController2, Level level) {
		// TODO Auto-generated constructor stub
	}

	// Konstruktor
	public Highscore(AttemptController attemptController, int level_id, int game_id) {
		this.attemptController = attemptController;
		this.level_id = level_id;

		// Zweidimensioaler Vector, mit Inhalt der Tabelle wird geholt.
		Vector<Vector<String>> vecRow = attemptController.getAllAttemptsPerLevel(level_id, game_id);

		int mark = attemptController.getPlayerPosition();

		setLayout(new BorderLayout(5, 10));

		// Spalten�berschriften
		Vector<String> title = new Vector<>();
		title.add("Rang");
		title.add("Spieler");
		title.add("Datum");
		title.add("Uhrzeit");
		title.add("Trefferwert");
		title.add("Genauigkeitswert");
		title.add("Reaktionswert");
		title.add("Highscore-Wert");

		// Tablle basierend auf zweidimensionalem Vector
		JTable table = new JTable(vecRow, title);
		if (mark >= 0)
			table.setRowSelectionInterval(mark, mark);
		setMinimumSize(new Dimension(650, 500));
		getContentPane().add(new JScrollPane(table), BorderLayout.NORTH);
		setTitle("Highscore-Liste"); // Titel
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		pack();
		setVisible(true);

		JButton btnZielndern = new JButton("Highscoreliste l�schen");
		btnZielndern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnAendern_Clicked(arg0);
			}
		});

		JPanel pnlSouth = new JPanel();
		pnlSouth.add(btnZielndern);

		// f�gt Panel mit Button hinzu
		add(pnlSouth, BorderLayout.SOUTH); 
	}

	

	public void btnAendern_Clicked(ActionEvent evt) {
		this.attemptController.deleteHighscore(level_id);
		dispose();
	}
}